package com.javarush.task.task09.task0930;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Задача по алгоритмам
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(new String("Вишня\n" +
//                "1\n" +
//                "Боб\n" +
//                "3\n" +
//                "Яблоко\n" +
//                "2\n" +
//                "0\n" +
//                "Арбуз\n").getBytes())));
        ArrayList<String> list = new ArrayList<String>();
        while (true) {
            String s = reader.readLine();
            if (s == null || s.isEmpty()) break;
            list.add(s);
        }

        String[] array = list.toArray(new String[list.size()]);
        sort(array);

        for (String x : array) {
            System.out.println(x);
        }
    }

    public static void sort(String[] array) {
        //напишите тут ваш код
        Queue<String> w = new PriorityQueue<>();
        Queue<Integer> d = new PriorityQueue<>(Collections.reverseOrder());

        isGreaterThan("", "");

        for (String s : array) {
            if (!isNumber(s)) w.add(s);
            else d.add(Integer.parseInt(s));
        }
        for (int i = 0; i < array.length; i++) {
            if (!isNumber(array[i])) array[i] = w.poll();
            else array[i] = String.valueOf(d.poll());
        }
    }

    // Метод для сравнения строк: 'а' больше чем 'b'
    public static boolean isGreaterThan(String a, String b) {
        return a.compareTo(b) > 0;
    }


    // Переданная строка - это число?
    public static boolean isNumber(String s) {
        if (s.length() == 0) return false;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if ((i != 0 && c == '-') // есть '-' внутри строки
                    || (!Character.isDigit(c) && c != '-')) // не цифра и не начинается с '-'
            {
                return false;
            }
        }
        return true;
    }
}

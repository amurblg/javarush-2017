package com.javarush.task.task09.task0929;

import java.io.*;

/* 
Обогатим код функциональностью!
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(new String("k:\\3.txt\nk:\\1.txt\nk:\\2.txt").getBytes())));

        String sourceFileName;
//        while (true) {
        sourceFileName = reader.readLine();
//            if ((new File(sourceFileName)).exists()) break;
//            System.out.println("Файл не существует.");
//        }

        String destinationFileName = reader.readLine();

        InputStream fileInputStream/* = getInputStream(sourceFileName)*/;
        while (true) {
            try {
                fileInputStream = getInputStream(sourceFileName);
                break;
            } catch (Exception e) {
                System.out.println("Файл не существует.");
                sourceFileName = reader.readLine();
            }
        }
        OutputStream fileOutputStream = getOutputStream(destinationFileName);

        while (fileInputStream.available() > 0) {
            int data = fileInputStream.read();
            fileOutputStream.write(data);
        }

        fileInputStream.close();
        fileOutputStream.close();
    }

    public static InputStream getInputStream(String fileName) throws IOException {
        return new FileInputStream(fileName);
    }

    public static OutputStream getOutputStream(String fileName) throws IOException {
        return new FileOutputStream(fileName);
    }
}


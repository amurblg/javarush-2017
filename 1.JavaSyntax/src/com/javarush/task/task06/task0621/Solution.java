package com.javarush.task.task06.task0621;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Родственные связи кошек
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(new String("дедушка Вася\n" +
//                "бабушка Мурка\n" +
//                "папа Котофей\n" +
//                "мама Василиса\n" +
//                "сын Мурчик\n" +
//                "дочь Пушинка").getBytes())));

        Cat gf = new Cat(reader.readLine());
        Cat gm = new Cat(reader.readLine());

        Cat f = new Cat(reader.readLine(), gf, null);
        Cat m = new Cat(reader.readLine(), null, gm);

        Cat s = new Cat(reader.readLine(), f, m);
        Cat d = new Cat(reader.readLine(), f, m);

        System.out.println(gf);
        System.out.println(gm);
        System.out.println(f);
        System.out.println(m);
        System.out.println(s);
        System.out.println(d);
    }

    public static class Cat {
        private String name;
        private Cat mother;
        private Cat father;

        Cat(String name) {
            this.name = name;
        }

        Cat(String name, Cat father, Cat mother) {
            this.name = name;
            this.mother = mother;
            this.father = father;
        }

        @Override
        public String toString() {
            return "Cat name is " + name + ", " +
                    (mother == null ? "no mother, " : "mother is " + mother.name + ", ") +
                    (father == null ? "no father" : "father is " + father.name);
        }
    }

}

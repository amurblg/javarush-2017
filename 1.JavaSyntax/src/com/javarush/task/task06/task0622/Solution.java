package com.javarush.task.task06.task0622;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/* 
Числа по возрастанию
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(new String("3\n" +
//                "2\n" +
//                "15\n" +
//                "6\n" +
//                "17").getBytes())));

        //напишите тут ваш код
        int[] array = new int[5];
        for (int i = 0; i < 5; i++) {
            array[i] = Integer.parseInt(reader.readLine());
        }
        Arrays.sort(array);
        Arrays.stream(array).forEach(e -> System.out.println(e));
    }
}

package com.javarush.task.task10.task1019;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Функциональности маловато!
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        HashMap<String, Integer> map = new HashMap<>();
        while (true) {
            String s1 = reader.readLine();
            if (s1 == null || s1.length() == 0) break;
            String s2 = reader.readLine();
            if (s2 == null || s2.length() == 0) break;
            map.put(s2, Integer.parseInt(s1));
        }

        for (Map.Entry<String, Integer> stringIntegerEntry : map.entrySet()) {
            System.out.println(stringIntegerEntry.getValue() + " " + stringIntegerEntry.getKey());
        }
    }
}

package com.javarush.task.task08.task0828;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/* 
Номер месяца
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        List<String> months = new ArrayList<>(12);
        months.addAll(Arrays.asList("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"));
        Scanner scanner = new Scanner(System.in);
        String m = scanner.nextLine();
        System.out.printf("%s is %d month", m, 1+months.indexOf(m));
        ;
    }
}

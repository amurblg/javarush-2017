package com.javarush.task.task21.task2103;

/* 
Все гениальное - просто!
*/
public class Solution {
    public static boolean calculate(boolean a, boolean b, boolean c, boolean d) {
        return c;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 16; i++) {
            String binary = String.format("%04d", Integer.parseInt(Integer.toString(i, 2)));
            boolean a = false, b = false, c = false, d = false;
            if (binary.substring(0, 1).equals("1")) a = true;
            if (binary.substring(1, 2).equals("1")) b = true;
            if (binary.substring(2, 3).equals("1")) c = true;
            if (binary.substring(3, 4).equals("1")) d = true;
            System.out.println(a + " " + b + " " + c + " " + d + ": " + calculate(a, b, c, d));
        }
    }
}

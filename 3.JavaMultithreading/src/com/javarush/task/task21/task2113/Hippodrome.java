package com.javarush.task.task21.task2113;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Registered on 16.07.2017.
 */
public class Hippodrome {
    private List<Horse> horses;
    static Hippodrome game;

    public List<Horse> getHorses() {
        return horses;
    }

    public Hippodrome(List<Horse> horses) {
        this.horses = horses;
    }

    public static void main(String[] args) throws InterruptedException {
        game = new Hippodrome(Arrays.asList(new Horse("h1", 3, 0), new Horse("h2", 3, 0), new Horse("h3", 3, 0)));
        game.run();
        game.printWinner();
    }

    public Horse getWinner() {
        Horse result = horses.get(0);
        for (Horse horse : horses) {
            if (horse.getDistance() > result.getDistance()) result = horse;
        }
        return result;
    }

    public void printWinner() {
        System.out.printf("Winner is %s!", getWinner().getName());
    }

    public void run() throws InterruptedException {
        for (int i = 1; i <= 100; i++) {
            move();
            print();
            Thread.sleep(200);
        }
    }

    public void print() {
        for (Horse horse : horses) {
            horse.print();
        }
        System.out.println("\n\n\n\n\n\n\n\n\n");
    }

    public void move() {
        for (Horse horse : horses) {
            horse.move();
        }
    }
}

package com.javarush.task.task29.task2907;

import java.math.BigDecimal;

/* 
Этот странный BigDecimal
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getValue(1.1d, 1.2d));
    }

    public static BigDecimal getValue(double v1, double v2) {
        //return new BigDecimal(String.valueOf(v1)).add(new BigDecimal(String.valueOf(v2)));//2.3
        return new BigDecimal(v1).add(new BigDecimal(v2));//2.3000000000000000444089209850062616169452667236328125
    }
}
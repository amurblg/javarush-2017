package com.javarush.task.task29.task2913;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/* 
Замена рекурсии
*/

public class Solution {
    private static int numberA;
    private static int numberB;

    public static String getAllNumbersBetween(int a, int b) {
//        if (a > b) {
//            return a + " " + getAllNumbersBetween(a - 1, b);
//        } else {
//            if (a == b) {
//                return Integer.toString(a);
//            }
//            return a + " " + getAllNumbersBetween(a + 1, b);
//        }
        if (a < b)
            return String.join(" ", IntStream.rangeClosed(a, b).boxed().map(String::valueOf).collect(Collectors.toList()));
        else if (a > b)
            return String.join(" ", IntStream.rangeClosed(b, a).map(i -> b - i + a).boxed().map(String::valueOf).collect(Collectors.toList()));
        else return Integer.toString(a);
    }

    public static void main(String[] args) {
        Random random = new Random();
        numberA = random.nextInt() % 1_000;
        numberB = random.nextInt() % 10_000;
        System.out.println(getAllNumbersBetween(numberA, numberB));
        System.out.println(getAllNumbersBetween(numberB, numberA));
    }
}
package com.javarush.task.task30.task3008.client;

import com.javarush.task.task30.task3008.ConsoleHelper;
import com.javarush.task.task30.task3008.Message;
import com.javarush.task.task30.task3008.MessageType;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BotClient extends Client {
    public static void main(String[] args) {
        BotClient bot = new BotClient();
        bot.run();
    }

    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    @Override
    protected String getUserName() {
        int i = (int) (Math.random() * 100);
        return "date_bot_" + i;
    }

    public class BotSocketThread extends Client.SocketThread {
        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }

        @Override
        protected void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
            String[] data = message.split(": ");
            SimpleDateFormat sdf;
            if (data.length == 2) {
                Date date = new Date();
                if (data[1].equals("дата")) {
                    sdf = new SimpleDateFormat("d.MM.YYYY");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                } else if (data[1].equals("день")) {
                    sdf = new SimpleDateFormat("d");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                } else if (data[1].equals("месяц")) {
                    sdf = new SimpleDateFormat("MMMM");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                } else if (data[1].equals("год")) {
                    sdf = new SimpleDateFormat("YYYY");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                } else if (data[1].equals("время")) {
                    sdf = new SimpleDateFormat("H:mm:ss");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                } else if (data[1].equals("час")) {
                    sdf = new SimpleDateFormat("H");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                } else if (data[1].equals("минуты")) {
                    sdf = new SimpleDateFormat("m");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                } else if (data[1].equals("секунды")) {
                    sdf = new SimpleDateFormat("s");
                    sendTextMessage(String.format("Информация для %s: %s", data[0], sdf.format(date)));
                }
            }
        }
    }
}

package com.javarush.task.task30.task3008;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        int port = ConsoleHelper.readInt();
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Сервер запущен");
            while (true) {
                Socket socket = serverSocket.accept();
                Handler handler = new Handler(socket);
                handler.start();
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (!serverSocket.isClosed()) try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void sendBroadcastMessage(Message message) {
        for (Map.Entry<String, Connection> entry : connectionMap.entrySet()) {
            try {
                entry.getValue().send(message);
            } catch (IOException e) {
                ConsoleHelper.writeMessage("Ошибка отправки сообщения: " + message.toString());
            }
        }
    }

    private static class Handler extends Thread {
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {
            while (true) {
                Message nameRequest = new Message(MessageType.NAME_REQUEST);
                connection.send(nameRequest);
                Message nameResponse = connection.receive();
                if (nameResponse.getType() != MessageType.USER_NAME) continue;
                String userName = nameResponse.getData();
                if (userName.trim().length() == 0) continue;
                if (connectionMap.containsKey(userName)) continue;
                connectionMap.put(userName, connection);
                connection.send(new Message(MessageType.NAME_ACCEPTED));
                return userName;
            }
        }

        private void sendListOfUsers(Connection connection, String userName) throws IOException {
            for (String s : connectionMap.keySet()) {
                if (!s.equals(userName)) {
                    Message msg = new Message(MessageType.USER_ADDED, s);
                    connection.send(msg);
                }
            }
        }

        @Override
        public void run() {
            System.out.println("Установлено новое соединение с удаленным адресом: " + socket.getRemoteSocketAddress());
            try (Connection connection = new Connection(socket)) {
                String userName = serverHandshake(connection);
                Server.sendBroadcastMessage(new Message(MessageType.USER_ADDED, userName));
                sendListOfUsers(connection, userName);
                serverMainLoop(connection, userName);

                connectionMap.remove(userName);
                Server.sendBroadcastMessage(new Message(MessageType.USER_REMOVED, userName));
                System.out.println("Соединение с удаленным адресом закрыто");
            } catch (IOException e) {
                System.out.println("Произошла ошибка при обмене данными с удаленным адресом");
            } catch (ClassNotFoundException e) {
                System.out.println("Произошла ошибка при обмене данными с удаленным адресом");
            }
        }

        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException {
            while (true) {
                Message msg = connection.receive();
                if (msg.getType() == MessageType.TEXT) {
                    Message toAll = new Message(MessageType.TEXT, String.format("%s: %s", userName, msg.getData()));
                    Server.sendBroadcastMessage(toAll);
                } else {
                    System.out.println("Ошибка! Тип сообщения: " + msg.getType());
                }
            }
        }
    }
}

package com.javarush.task.task30.task3004;

import java.util.concurrent.RecursiveTask;

public class BinaryRepresentationTask extends RecursiveTask<String> {
    private int x;
    private StringBuilder result = new StringBuilder();

    public BinaryRepresentationTask(int x) {
        this.x = x;
    }

    @Override
    protected String compute() {
        result.append(x % 2);
        x = x / 2;
        if (x > 0) {
            BinaryRepresentationTask next = new BinaryRepresentationTask(x);
            next.fork();
            return result.insert(0, next.join()).toString();
        }

        return result.toString();
    }
}

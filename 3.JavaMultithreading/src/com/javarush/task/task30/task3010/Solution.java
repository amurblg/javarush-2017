package com.javarush.task.task30.task3010;

/* 
Минимальное допустимое основание системы счисления
*/

import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        try {
            if (!args[0].matches("^[0-9a-zA-Z]+$")) {
                System.out.println("incorrect");
                return;
            }
            char[] chars = args[0].toLowerCase().toCharArray();
            Arrays.sort(chars);
            if (chars[chars.length - 1] > 96) System.out.println(chars[chars.length - 1] - 86);
            else if (chars[chars.length - 1] < 50) System.out.println(2);
            else System.out.println(chars[chars.length - 1] - 47);
        } catch (Exception e) {
        }
    }
}
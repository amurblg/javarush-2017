package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.kitchen.Dish;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConsoleHelper {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static String readString() throws IOException {
        return reader.readLine();
    }

    public static List<Dish> getAllDishesForOrder() throws IOException {
        writeMessage(Dish.allDishesToString());
        writeMessage("Введите блюдо, или exit для выхода:");
        String dish;
        List<Dish> result = new ArrayList<>();
        List<String> existed = Arrays.asList(Dish.allDishesToString().split(", "));
        while (!(dish = readString()).equals("exit")) {
            if (existed.contains(dish)) result.add(Dish.valueOf(dish));
            else writeMessage("Такого блюда нет.");
        }

        return result;
    }
}

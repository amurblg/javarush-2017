package com.javarush.task.task24.task2409;

/**
 * Created by Registered on 18.07.2017.
 */
public interface Jeans extends Item{
    int getLength();
    int getSize();
}

package com.javarush.task.task35.task3513;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Model {
    private static final int FIELD_WIDTH = 4;
    private Tile[][] gameTiles;

    int score = 0;
    int maxTile = 2;

    public Model() {
        resetGameTiles();
    }

    void resetGameTiles() {
        gameTiles = new Tile[FIELD_WIDTH][FIELD_WIDTH];
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                gameTiles[i][j] = new Tile();
            }
        }
        addTile();
        addTile();
    }

    private void addTile() {
        List<Tile> emptyTiles = getEmptyTiles();
        if (emptyTiles != null && emptyTiles.size() > 0)
            emptyTiles.get((int) (Math.random() * emptyTiles.size())).value = (Math.random() < 0.9 ? 2 : 4);
    }

    private List<Tile> getEmptyTiles() {
        List<Tile> result = new ArrayList<>();
        for (int i = 0; i < FIELD_WIDTH; i++) {
            for (int j = 0; j < FIELD_WIDTH; j++) {
                if (gameTiles[i][j].isEmpty()) result.add(gameTiles[i][j]);
            }
        }

        return result;
    }

    private boolean compressTiles(Tile[] tiles) {
        List<Tile> tmp = new ArrayList<>(Arrays.asList(tiles));
        if (tmp.stream().filter(e -> e.value > 0).count() == 0) return false;
        boolean hasChanged = false;
        for (int i = 0, j = 0; i < tiles.length - 1; i++) {
            if (tmp.get(j).value == 0 && tmp.subList(j, tmp.size()).stream().filter(e -> e.value != 0).count() > 0) {
                hasChanged = true;
                tmp.remove(j);
                tmp.add(new Tile());
            }
            if (tmp.get(j).value != 0) j++;
        }
        for (int i = 0; i < tiles.length; i++) {
            tiles[i] = tmp.get(i);
        }
        return hasChanged;
    }

    private boolean mergeTiles(Tile[] tiles) {
        boolean hasChanged = false;
        for (int i = 0; i < tiles.length - 2; i++) {
            if (tiles[i + 1].value == tiles[i].value && tiles[i].value > 0) {
                hasChanged = true;
                tiles[i].value *= 2;
                if (tiles[i].value > maxTile) maxTile = tiles[i].value;
                score += tiles[i].value;
                for (int j = i + 1; j < tiles.length - 1; j++) {
                    tiles[j] = tiles[j + 1];
                }
                tiles[tiles.length - 1] = new Tile();
            }
        }

        return hasChanged;
    }

    public void left() {
        boolean hasChanged = false;
        for (int i = 0; i < gameTiles.length; i++) {
            hasChanged |= compressTiles(gameTiles[i]);
            hasChanged |= mergeTiles(gameTiles[i]);
        }
        if (hasChanged) addTile();
    }


    public void right() {
        rotateGameTilesClockwise(2);
        boolean hasChanged = false;
        for (int i = 0; i < gameTiles.length; i++) {
            hasChanged |= compressTiles(gameTiles[i]);
            hasChanged |= mergeTiles(gameTiles[i]);
        }
        if (hasChanged) addTile();
        rotateGameTilesClockwise(2);
    }

    public void up() {
        rotateGameTilesClockwise(3);
        boolean hasChanged = false;
        for (int i = 0; i < gameTiles.length; i++) {
            hasChanged |= compressTiles(gameTiles[i]);
            hasChanged |= mergeTiles(gameTiles[i]);
        }
        if (hasChanged) addTile();
        rotateGameTilesClockwise(1);
    }

    public void down() {
        rotateGameTilesClockwise(1);
        boolean hasChanged = false;
        for (int i = 0; i < gameTiles.length; i++) {
            hasChanged |= compressTiles(gameTiles[i]);
            hasChanged |= mergeTiles(gameTiles[i]);
        }
        if (hasChanged) addTile();
        rotateGameTilesClockwise(3);
    }

    private void rotateGameTilesClockwise(int numberOfTimes) {
        Tile[][] copy = new Tile[gameTiles.length][gameTiles[0].length];
        for (int i = 0; i < gameTiles.length; i++) {
            copy[i] = Arrays.copyOf(gameTiles[i], gameTiles[i].length);
        }
        for (int k = 0; k < numberOfTimes; k++) {
            for (int i = 0; i < gameTiles.length; i++) {
                for (int j = 0; j < gameTiles[0].length; j++) {
                    gameTiles[j][gameTiles[0].length - i - 1] = copy[i][j];
                }
            }
        }
    }
}

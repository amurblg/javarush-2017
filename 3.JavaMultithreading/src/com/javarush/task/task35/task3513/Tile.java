package com.javarush.task.task35.task3513;

import java.awt.*;

public class Tile {
    int value;

    public Tile() {
        this(0);
    }

    public Tile(int value) {

        this.value = value;
    }

    public boolean isEmpty() {
        return (value == 0);
    }

    public Color getFontColor() {
        return new Color(value < 16 ? 0x776e65 : 0xf9f6f2);
    }

    public Color getTileColor() {
        int result = 0xff0000;
        switch (value) {
            case 0:
                result = 0xcdc1b4;
                break;
            case 2:
                result = 0xeee4da;
                break;
            case 4:
                result = 0xede0c8;
                break;
            case 8:
                result = 0xf2b179;
                break;
            case 16:
                result = 0xf59563;
                break;
            case 32:
                result = 0xf67c5f;
                break;
            case 64:
                result = 0xf65e3b;
                break;
            case 128:
                result = 0xedcf72;
                break;
            case 256:
                result = 0xedcc61;
                break;
            case 512:
                result = 0xedc850;
                break;
            case 1024:
                result = 0xedc53f;
                break;
            case 2048:
                result = 0xedc22e;
                break;
        }

        return new Color(result);
    }
}

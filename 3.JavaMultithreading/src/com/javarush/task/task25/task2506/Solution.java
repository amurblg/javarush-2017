package com.javarush.task.task25.task2506;

/* 
Мониторинг состояния нити
*/
public class Solution {
    public static void main(String[] args) throws InterruptedException {
        Thread target = new Thread();
        LoggingStateThread loggingStateThread = new LoggingStateThread(target);

        loggingStateThread.start();
        target.start();  //NEW
        Thread.sleep(100); //RUNNABLE
        target.join(100);
        Thread.sleep(400);
        target.interrupt(); //TERMINATED
        Thread.sleep(500);
    }
}

class LoggingStateThread extends Thread {
    Thread target;

    @Override
    public void run() {
        //super.run();
        Thread.State currentState;
        currentState = target.getState();
        System.out.println(currentState);
        while (true) {
            if (target.getState() != currentState) {
                System.out.println(target.getState());
                currentState = target.getState();
                if (currentState == State.TERMINATED) break;
            }
        }
    }

    public LoggingStateThread(Thread target) {
        this.target = target;
    }
}

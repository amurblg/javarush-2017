package com.javarush.task.task25.task2502;

import java.util.*;

/* 
Машину на СТО не повезем!
*/
public class Solution {
    public static enum Wheel {
        FRONT_LEFT,
        FRONT_RIGHT,
        BACK_LEFT,
        BACK_RIGHT
    }

    public static class Car {
        protected List<Wheel> wheels;

        public Car() {
            //init wheels here
            String[] newWheels = loadWheelNamesFromDB();
            wheels = new ArrayList<>();
            List<Wheel> correctSet = new ArrayList<>(Arrays.asList(Wheel.BACK_LEFT, Wheel.BACK_RIGHT, Wheel.FRONT_LEFT, Wheel.FRONT_RIGHT));
            for (int i = 0; i < newWheels.length; i++) {
                if (!correctSet.contains(Wheel.valueOf(newWheels[i]))) throw new IllegalArgumentException();
                correctSet.remove(Wheel.valueOf(newWheels[i]));
                wheels.add(Wheel.valueOf(newWheels[i]));
            }
            if (correctSet.size() != 0) throw new IllegalArgumentException();
        }

        protected String[] loadWheelNamesFromDB() {
            //this method returns mock data
            return new String[]{"FRONT_LEFT", "FRONT_RIGHT", "BACK_LEFT", "BACK_RIGHT"};
        }
    }

    public static void main(String[] args) {
        Car c = new Car();
    }
}

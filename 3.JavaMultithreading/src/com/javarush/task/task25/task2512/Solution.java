package com.javarush.task.task25.task2512;

import java.util.Stack;

/*
Живем своим умом
*/
public class Solution implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        t.interrupt();
        Stack<Throwable> stack = new Stack<>();
        stack.push(e);
        while (e.getCause() != null) {
            stack.push(e.getCause());
            e = e.getCause();
        }
        while (stack.size() > 0) {
            System.out.println(stack.peek().getClass().toString().substring(6) + ": " + stack.peek().getMessage());
            stack.pop();
        }
    }

    public static void main(String[] args) throws Exception {
        Solution s = new Solution();
        Thread.currentThread().setUncaughtExceptionHandler(s);
        throw new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI")));
    }
}

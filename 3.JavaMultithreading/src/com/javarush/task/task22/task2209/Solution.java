package com.javarush.task.task22.task2209;

import java.io.*;
import java.util.*;

/*
Составить цепочку слов
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        //...
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
        List<String> cities = new ArrayList<>();
        while (fileReader.ready()) {
            String[] nextLine = fileReader.readLine().split(" +");
            for (int i = 0; i < nextLine.length; i++) {
                cities.add(nextLine[i]);
            }
        }
        reader.close();

        String[] array = new String[cities.size()];
        for (int i = 0; i < cities.size(); i++) {
            array[i] = cities.get(i);
        }

        System.out.println(getLine(array));
    }

    public static StringBuilder getLine(String... words) {
        initOrganizedWordsList(Arrays.asList(words));
        if (words == null || words.length == 0) return new StringBuilder();
        return new StringBuilder(String.join(" ", constructChain()));
    }

    private static List<String> constructChain() {
        if (organizedWords == null || organizedWords.size() == 0) return null;
        if (organizedWords.size() == 1) return new ArrayList<String>(Arrays.asList(organizedWords.get(0).getWord()));

        organizedWords.stream().filter(e -> e.getCandidatesToHead().size() + e.getCandidatesToTail().size() == 0).forEach(e -> e.setUsedInChain(true));
        int activeWords = (int) organizedWords.stream().filter(e -> e.isUsedInChain() == false).count();
        if (activeWords == 0) return new ArrayList<String>(Arrays.asList((organizedWords.get(0).getWord())));

        List<String> result = new ArrayList<>();
        if (activeWords == 1) {
            organizedWords.stream().filter(e -> e.usedInChain == false).forEach(e -> result.add(e.getWord()));
            return result;
        }

        MyWord lastUsed = organizedWords.stream().filter(e -> e.usedInChain == false).findFirst().get();
        boolean addToTail = true;
        while (activeWords > 0) {
            result.add(addToTail ? result.size() : 0, lastUsed.getWord());
            lastUsed.setUsedInChain(true);
            if (lastUsed.getCandidatesToTail().stream().filter(e -> e.usedInChain == false).count() > 0) {
                lastUsed = lastUsed.getCandidatesToTail().stream().filter(e -> e.usedInChain == false).findFirst().get();
                addToTail = true;
            } else if (lastUsed.getCandidatesToHead().stream().filter(e -> e.usedInChain == false).count() > 0) {
                lastUsed = lastUsed.getCandidatesToHead().stream().filter(e -> e.usedInChain == false).findFirst().get();
                addToTail = false;
            }

            activeWords = (int) organizedWords.stream().filter(e -> e.isUsedInChain() == false).count();
        }

        return result;
    }

    private static List<MyWord> organizedWords = new ArrayList<>();

    private static void initOrganizedWordsList(List<String> wordsList) {
        for (String word : wordsList) {
            organizedWords.add(new MyWord(word));
        }

        for (int i = 0; i < organizedWords.size(); i++) {
            String word = organizedWords.get(i).getWord().toLowerCase();
            for (int j = 0; j < organizedWords.size(); j++) {
                if (i != j) {
                    String nextWord = organizedWords.get(j).getWord().toLowerCase();
                    if (word.toCharArray()[0] == nextWord.toCharArray()[nextWord.length() - 1])
                        organizedWords.get(i).getCandidatesToHead().add(organizedWords.get(j));
                    if (word.toCharArray()[word.length() - 1] == nextWord.toCharArray()[0])
                        organizedWords.get(i).getCandidatesToTail().add(organizedWords.get(j));
                }
            }
        }

        Collections.sort(organizedWords, new Comparator<MyWord>() {
            @Override
            public int compare(MyWord o1, MyWord o2) {
                int o1combinedSize = o1.getCandidatesToHead().size() + o1.getCandidatesToTail().size();
                int o2combinedSize = o2.getCandidatesToHead().size() + o2.getCandidatesToTail().size();
                return o1combinedSize == o2combinedSize ? o1.getWord().compareTo(o2.getWord()) : o1combinedSize - o2combinedSize;
            }
        });
    }

    public static class MyWord {
        private String word;
        private List<MyWord> candidatesToTail;
        private List<MyWord> candidatesToHead;
        private boolean usedInChain = false;

        public boolean isUsedInChain() {
            return usedInChain;
        }

        public void setUsedInChain(boolean usedInChain) {
            this.usedInChain = usedInChain;
        }

        public String getWord() {
            return word;
        }

        public List<MyWord> getCandidatesToTail() {
            return candidatesToTail;
        }

        public List<MyWord> getCandidatesToHead() {
            return candidatesToHead;
        }

        @Override
        public String toString() {
            return "MyWord{" +
                    "word='" + word + '\'' +
                    ", candidatesToTail=" + candidatesToTail.size() +
                    ", candidatesToHead=" + candidatesToHead.size() +
                    '}';
        }

        public MyWord(String word) {
            this.word = word;
            candidatesToHead = new ArrayList<>();
            candidatesToTail = new ArrayList<>();
        }
    }
}

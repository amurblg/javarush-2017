package com.javarush.task.task22.task2212;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Проверка номера телефона
*/
public class Solution {
    public static boolean checkTelNumber(String telNumber) {
        if (telNumber == null || telNumber.isEmpty()) return false;
        if (telNumber.startsWith("+") && countOccurrences(telNumber, "\\d") != 12) return false;
        if (countOccurrences(telNumber.substring(0, 1), "\\d|\\(") == 1 && countOccurrences(telNumber, "\\d") != 10)
            return false;
        if (countOccurrences(telNumber, "\\-") > 2) return false;
        if (countOccurrences(telNumber, "\\--") > 0) return false;
        if (countOccurrences(telNumber, "\\(") > 1) return false;
        if (countOccurrences(telNumber, "\\(") == 1 && countOccurrences(telNumber, "\\(\\d{3}\\)") == 0) return false;
        if (telNumber.indexOf("(") != -1 && telNumber.indexOf("-") != -1 && (telNumber.indexOf("(") > telNumber.indexOf("-")))
            return false;
        if (countOccurrences(telNumber, "[^\\+\\-\\(\\)0-9]") > 0) return false;
        if (countOccurrences(telNumber, "[0-9]$") != 1) return false;

        return true;
    }

    public static void main(String[] args) {
    }

    private static int countOccurrences(String string, String regexPattern) {
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(string);
        int count = 0;
        while (matcher.find()) count++;

        return count;
    }
}

package com.javarush.task.task22.task2211;

import java.io.*;
import java.nio.charset.Charset;

/* 
Смена кодировки
*/
public class Solution {
    //static String win1251TestString = "РќР°СЂСѓС€РµРЅРёРµ РєРѕРґРёСЂРѕРІРєРё РєРѕРЅСЃРѕР»Рё?"; //only for your testing

    public static void main(String[] args) throws IOException {
        InputStreamReader streamReader = new InputStreamReader(new FileInputStream(args[0]), "UTF-8");
        OutputStreamWriter streamWriter = new OutputStreamWriter(new FileOutputStream(args[1]), "Windows-1251");
        while (streamReader.ready()) {
            streamWriter.write(streamReader.read());
        }
        streamReader.close();
        streamWriter.close();
    }
}

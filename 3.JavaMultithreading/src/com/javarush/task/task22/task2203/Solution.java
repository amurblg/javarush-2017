package com.javarush.task.task22.task2203;

/* 
Между табуляциями
*/
public class Solution {
    public static String getPartOfString(String string) throws TooShortStringException {
        if (string == null || string.length() == 0) throw new TooShortStringException();
        String[] parts = string.split("\t");
        int i = 0;
        int counter = 0;
        do {
            i = string.indexOf("\t", i) + 1;
            counter++;
        } while (i != 0);
        if ((counter - 1) < 2) throw new TooShortStringException();
        return parts[1];
    }

    public static class TooShortStringException extends Exception {
    }

    public static void main(String[] args) throws TooShortStringException {
        //System.out.println(getPartOfString("\tJavaRush - лучший сервис \tобучения Java\t."));
        System.out.println(getPartOfString("2\tJavaRush - лучший сервис\t1"));
    }
}

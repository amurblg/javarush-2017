package com.javarush.task.task22.task2202;

/*
Найти подстроку
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getPartOfString("JavaRush - лучший сервис обучения Java."));
        //System.out.println(getPartOfString("JavaRush - лучший сервис обучения"));
    }

    public static String getPartOfString(String string) {
        if (string == null || string.length() == 0) throw new TooShortStringException();
        String result = "";
        try {
            int fourthSpace = 0;
            for (int i = 0; i < 4; i++) {
                fourthSpace = string.indexOf(" ", fourthSpace) + 1;
            }
            if (fourthSpace == -1) throw new TooShortStringException();
            //result = string.substring(string.indexOf(" ") + 1, string.indexOf(" ", fourthSpace + 1));
            String[] words = string.split(" +");
            result = String.join(" ", words[1], words[2], words[3], words[4]);
        } catch (Exception e) {
            throw new TooShortStringException();
        }

        return result;
    }

    public static class TooShortStringException extends RuntimeException {
    }
}

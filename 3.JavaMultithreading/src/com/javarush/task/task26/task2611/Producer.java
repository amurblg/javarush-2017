package com.javarush.task.task26.task2611;

import java.util.concurrent.ConcurrentHashMap;

public class Producer implements Runnable {
    private ConcurrentHashMap<String, String> map;

    public Producer(ConcurrentHashMap<String, String> map) {
        this.map = map;
    }

    @Override
    public void run() {
        int i = 1;
        while (true) {
            try {
                map.put(Integer.toString(i), String.format("Some text for %d", i++));
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.printf("[%s] thread was terminated\n", Thread.currentThread().getName());
                break;
            }
        }
    }
}

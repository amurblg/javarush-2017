package com.javarush.task.task20.task2027;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* 
Кроссворд
*/
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };
        detectAllWords(crossword, "home", "same", "red", "ran", "leo", "dar", "oprek", "luf", "vorg");
        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        List<Word> result = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            String currentWord = words[i];
            char firstLetter = currentWord.toCharArray()[0];
            boolean checkNextWord = false;
            for (int j = 0; j < crossword.length; j++) {
                for (int k = 0; k < crossword[0].length; k++) {
                    if (crossword[j][k] == firstLetter) {
                        Directions determinedDirection = determineRightDirection(crossword, j, k, currentWord);
                        if (determinedDirection == null) continue;
                        else {
                            Word w = new Word(currentWord);
                            w.setStartPoint(k, j);
                            w.setEndPoint(k + (currentWord.length() - 1) * possibleDirections.get(determinedDirection).getCol(), j + (currentWord.length() - 1) * possibleDirections.get(determinedDirection).getRow());
                            result.add(w);
                            //System.out.printf("%s - (%d, %d) - (%d, %d)\n", currentWord, k, j, k + (currentWord.length() - 1) * possibleDirections.get(determinedDirection).getCol(), j + (currentWord.length() - 1) * possibleDirections.get(determinedDirection).getRow());
                            checkNextWord = true;
                            break;
                        }
                    }
                }
                if (checkNextWord) break;
            }
        }

        return result;
    }

    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }

    private static class Pair {
        private int row;
        private int col;

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public Pair(int row, int col) {

            this.row = row;
            this.col = col;
        }
    }

    private static enum Directions {
        N, NE, E, SE, S, SW, W, WE
    }

    private static Map<Directions, Pair> possibleDirections = new HashMap<>();

    private static Directions determineRightDirection(int[][] crossword, int row, int col, String word) {
        for (Directions direction : possibleDirections.keySet()) {
            boolean rightDirection = true;
            int currentRow;
            int currentCol;
            for (int i = 0; i < word.length(); i++) {
                currentRow = row + possibleDirections.get(direction).getRow() * i;
                currentCol = col + possibleDirections.get(direction).getCol() * i;
                if (currentRow < 0 || currentRow >= crossword.length || currentCol < 0 || currentCol >= crossword[0].length) {
                    rightDirection = false;
                    break;
                }
                rightDirection &= word.toCharArray()[i] == crossword[currentRow][currentCol];
            }
            if (!rightDirection) continue;
            else return direction;
        }

        return null;
    }

    static {
        possibleDirections.put(Directions.N, new Pair(-1, 0));
        possibleDirections.put(Directions.NE, new Pair(-1, 1));
        possibleDirections.put(Directions.E, new Pair(0, 1));
        possibleDirections.put(Directions.SE, new Pair(1, 1));
        possibleDirections.put(Directions.S, new Pair(1, 0));
        possibleDirections.put(Directions.SW, new Pair(1, -1));
        possibleDirections.put(Directions.W, new Pair(0, -1));
        possibleDirections.put(Directions.WE, new Pair(-1, -1));
    }
}

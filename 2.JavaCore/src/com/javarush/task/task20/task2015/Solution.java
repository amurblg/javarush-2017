package com.javarush.task.task20.task2015;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* 
Переопределение сериализации
*/
public class Solution implements Serializable, Runnable {
    transient private Thread runner;
    private int speed;

    public Solution(int speed) {
        this.speed = speed;
        runner = new Thread(this);
        runner.start();
    }

    public void run() {
        // do something here, does not matter
        while (!runner.isInterrupted()) {
            System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date()) + " runner is active");
            try {
                runner.sleep(speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Переопределяем сериализацию.
     * Для этого необходимо объявить методы:
     * private void writeObject(ObjectOutputStream out) throws IOException
     * private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException
     * Теперь сериализация/десериализация пойдет по нашему сценарию :)
     */
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.runner = new Thread(this);
        this.runner.start();
    }

    public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
        Solution s1 = new Solution(2000);
        Thread.currentThread().sleep(5000);
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("k:/ser1.txt"));
        oos.writeObject(s1);
        System.out.println("serialized");
        s1.runner.interrupt();
        System.out.println("s1 interrupted");

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("k:/ser1.txt"));
        Solution s2 = (Solution) ois.readObject();
        System.out.println("readObject done");
    }
}

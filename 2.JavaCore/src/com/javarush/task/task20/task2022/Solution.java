package com.javarush.task.task20.task2022;

import java.io.*;

/* 
Переопределение сериализации в потоке
*/
public class Solution implements Serializable, AutoCloseable {
    transient private FileOutputStream stream;
    private String fileName;

    public Solution(String fileName) throws FileNotFoundException {
        this.fileName = fileName;
        this.stream = new FileOutputStream(fileName);
    }

    public void writeObject(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush();
    }

    //В данном случае процесс сериализации идет по умолчанию - данный метод не нужен, default и так будет вызван
    //Но валидатору этот метод нужен, поэтому оставил.
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
//        out.close();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.stream = new FileOutputStream(fileName, true);
//        in.close();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //Тестовый блок для проверки правильности работы
        Solution s1 = new Solution("k:/ser4.txt");
        s1.writeObject("test b4 serialization");

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("k:/ser5.txt"));
        oos.writeObject(s1);

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("k:/ser5.txt"));
        Solution restored = (Solution) ois.readObject();
        restored.writeObject("test after serialization");
    }
}
package com.javarush.task.task20.task2025;

import java.util.ArrayList;
import java.util.List;

/*
Алгоритмы-числа
*/
public class Solution {
    public static long[] getNumbers(long N) {
        class LocalSolver {
            List<Long> result = new ArrayList<>();
            byte digits = (byte) Long.toString(N).length();
            private long[][] matrix = new long[2][digits];
            private boolean[] recalc = new boolean[digits];

            public long[] getNumbers() {
                for (int i = 0; i < N; i++) {

                }


                long[] result = new long[this.result.size()];
                for (int i = 0; i < result.length; i++) result[i] = this.result.get(i);
                return result;
            }
        }
        return new LocalSolver().getNumbers();
    }
/*
    public static long[] getNumbers(long N) {
        long[] result = null;
        List<Long> list = new ArrayList<>();

        long[] powers = null;
        byte prevLength = 0;
        for (long i = 1; i < N; i++) {
            byte currentLength = getLength(i);
            if (prevLength != currentLength) {
                prevLength = currentLength;
                powers = new long[10];
                for (int j = 0; j < 10; j++) {
                    powers[j] = (long) Math.pow(j, currentLength);
                }
            }
            long sum = 0;
            for (byte b : getNumberDigits(i, currentLength)) {
                sum += powers[b];
            }
            if (i == sum) list.add(i);
        }

        result = new long[list.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = list.get(i);
        }

        return result;
    }
*/

    private static byte[] getNumberDigits(long number, byte length) {
        byte[] result = new byte[length];
        int i = 0;
        while (i < length) {
            result[length - i - 1] = (byte) (number % 10);
            number /= 10;
            i++;
        }

        return result;
    }

    private static byte getLength(long number) {
        byte result = 1;
        while ((number = number / 10) > 0) result++;
        return result;
    }

    private static byte getLengthByString(long number) {
        return (byte) Long.toString(number).length();
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
//        getNumbers((long) 1e8);

//        for (long i = 0; i < 1e9; i++) {
//            byte b = getLength(i);
//        }

        for (int i = 0; i < 1e8; i++) {
            byte[] b = getNumberDigits(i, (byte) 9);
        }
        System.out.printf("Finished in %d ms", System.currentTimeMillis() - start);
    }
}

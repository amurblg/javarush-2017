package com.javarush.task.task20.task2020;

import java.io.*;
import java.util.logging.Logger;

/* 
Сериализация человека
*/
public class Solution {

    public static class Person implements Serializable {
        String firstName;
        String lastName;
        transient String fullName;
        transient final String greetingString;
        String country;
        Sex sex;
        transient PrintStream outputStream;
        transient Logger logger;

        Person(String firstName, String lastName, String country, Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.greetingString = "Hello, ";
            this.country = country;
            this.sex = sex;
            this.outputStream = System.out;
            this.logger = Logger.getLogger(String.valueOf(Person.class));
        }

        private void writeObject(ObjectOutputStream oos) throws IOException {
            //System.out.println("writeObject!");
            oos.defaultWriteObject();
        }

        private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
            //System.out.println("readObject!");
            ois.defaultReadObject();
            this.fullName = String.format("%s, %s", lastName, firstName);
            //greetingString = "Hello, ";
        }
    }

    enum Sex {
        MALE,
        FEMALE
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Person p1 = new Person("John", "Smith", "Russia", Sex.MALE);
        System.out.println(p1.firstName);
        System.out.println(p1.lastName);
        System.out.println(p1.fullName);
        System.out.println(p1.greetingString);
        System.out.println(p1.logger);

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("k:/ser3.txt"));
        oos.writeObject(p1);
        System.out.println("---- serialized ----");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("k:/ser3.txt"));
        Person restored = (Person) ois.readObject();
        System.out.println(restored.firstName);
        System.out.println(restored.lastName);
        System.out.println(restored.fullName);
        System.out.println(restored.greetingString);
        System.out.println(restored.logger);
    }
}

package com.javarush.task.task20.task2026;

/* 
Алгоритмы-прямоугольники
*/
public class Solution {
    public static void main(String[] args) {
        byte[][] a = new byte[][]{
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 1}
        };
        int count = getRectangleCount(a);
        System.out.println("count = " + count + ". Должно быть 2");
    }

    public static int getRectangleCount(byte[][] a) {
        int result = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                if (a[i][j] == 1) {
                    result += 1;
                    markAllNeighboringCells(a, i, j);
                }
            }
        }
        return result;
    }

    private static void markAllNeighboringCells(byte[][] a, int row, int col) {
        if (row >= 0 && row < a.length && col >= 0 && col < a[0].length) {
            if (a[row][col] == 1) {
                a[row][col] = 2;
                for (byte i = -1; i <= 1; i++) {
                    for (byte j = -1; j <= 1; j++) {
                        markAllNeighboringCells(a, row + i, col + j);
                    }
                }
            }
        }
    }
}


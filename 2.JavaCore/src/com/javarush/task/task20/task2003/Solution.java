package com.javarush.task.task20.task2003;

import java.io.*;
import java.util.*;

/* 
Знакомство с properties
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();
    private static Properties props = new Properties();

    public void fillInPropertiesMap() throws Exception {
        //implement this method - реализуйте этот метод
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();

        load(new FileInputStream(fileName));
    }

    public void save(OutputStream outputStream) throws Exception {
        //implement this method - реализуйте этот метод
        props.putAll(properties);
        props.store(outputStream, null);
//        outputStream.close();
    }

    public void load(InputStream inputStream) throws Exception {
        //implement this method - реализуйте этот метод
        props.load(inputStream);
        Set<String> propNames = props.stringPropertyNames();
        for (String propName : propNames) {
            properties.put(propName, props.getProperty(propName));
        }
    }

    public static void main(String[] args) throws Exception {
//        properties.put("param1","value1");
//        properties.put("param2",null);
//        properties.put("param3","13");
//
//        Solution test = new Solution();
//
//        test.load(new FileInputStream("k:/props.txt"));
//        test.save(new FileOutputStream("k:/props2.txt"));
    }
}

package com.javarush.task.task19.task1907;

/* 
Считаем слово
*/

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        //BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream("k:/1.txt".getBytes())));
        String fileName = reader.readLine();
        reader.close();

        FileReader fr = new FileReader(fileName);
        char[] buffer = new char[(int) new File(fileName).length()];
        fr.read(buffer);
        fr.close();

        String data = new String(buffer);
        data = data.toLowerCase();
        Pattern p = Pattern.compile("\\bworld\\b");
        Matcher m = p.matcher(data);
        int counter = 0;
        while (m.find()) counter++;
        System.out.println(counter);
    }
}

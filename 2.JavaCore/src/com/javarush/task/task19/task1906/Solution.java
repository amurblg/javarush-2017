package com.javarush.task.task19.task1906;

/* 
Четные символы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        //BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream("k:/1.txt\nk:/2.txt".getBytes())));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        reader.close();

        FileReader fr = new FileReader(fileName1);
        char[] buffer = new char[(int) new File(fileName1).length()];
        fr.read(buffer);
        fr.close();

        FileWriter fw = new FileWriter(fileName2);
        for (int i = 1; i < buffer.length; i += 2) {
            fw.write(buffer[i]);
        }
        fw.close();
    }
}

package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName1 = "k:/lines1.txt";
//        String fileName2 = "k:/lines2.txt";
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        reader.close();

        List<String> file1Lines = new ArrayList<>();
        List<String> file2Lines = new ArrayList<>();

        BufferedReader reader1 = new BufferedReader(new FileReader(fileName1));
        while (reader1.ready()) file1Lines.add(reader1.readLine());
        reader1.close();

        BufferedReader reader2 = new BufferedReader(new FileReader(fileName2));
        while (reader2.ready()) file2Lines.add(reader2.readLine());
        reader2.close();

        while (file1Lines.size() > 0 && file2Lines.size() > 0) {
            if (file1Lines.get(0).equals(file2Lines.get(0))) {
                lines.add(new LineItem(Type.SAME, file1Lines.get(0)));
                file1Lines.remove(0);
                file2Lines.remove(0);
            } else {
                if (file2Lines.get(0).equals(file1Lines.get(1)) && !file1Lines.get(0).equals(file2Lines.get(1))) {
                    lines.add(new LineItem(Type.REMOVED, file1Lines.get(0)));
                    file1Lines.remove(0);
                } else {
                    lines.add(new LineItem(Type.ADDED, file2Lines.get(0)));
                    file2Lines.remove(0);
                }
            }
        }
        if (file1Lines.size() > 0) {
            while (file1Lines.size() > 0) {
                lines.add(new LineItem(Type.REMOVED, file1Lines.get(0)));
                file1Lines.remove(0);
            }
        } else if (file2Lines.size() > 0) {
            while (file2Lines.size() > 0) {
                lines.add(new LineItem(Type.ADDED, file2Lines.get(0)));
                file2Lines.remove(0);
            }
        }

        System.out.println(lines);
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
//        @Override
//        public String toString() {
//            return type + " " + line;
//        }
    }
}

package com.javarush.task.task19.task1912;

/* 
Ридер обертка 2
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream originalStream = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream decoy = new PrintStream(baos);
        System.setOut(decoy);

        testString.printSomething();
        String interception = baos.toString();
        interception = interception.replaceAll("te", "??");

        System.setOut(originalStream);
        System.out.println(interception);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
        }
    }
}

package com.javarush.task.task19.task1925;

/* 
Длинные слова
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {
        List<String> result = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        while (reader.ready()) {
            String[] nextPortion = reader.readLine().trim().split(" +");
            for (String s : nextPortion) {
                if (s.length() > 6) result.add(s);
            }
        }
        reader.close();

        BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
        writer.write(String.join(",", result));
        writer.close();
    }
}

package com.javarush.task.task19.task1908;

/* 
Выделяем числа
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        reader.close();

        List<String> list = new ArrayList<>();
        BufferedReader reader2 = new BufferedReader(new FileReader(fileName1));
        while (reader2.ready()) list.add(reader2.readLine());
        reader2.close();

        StringBuilder sb = new StringBuilder();
        Pattern p = Pattern.compile("\\b-?\\d+\\b");
        for (int i = 0; i < list.size(); i++) {
            Matcher m = p.matcher(list.get(i));
            while (m.find()) sb.append(m.group()).append(" ");
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName2));
        writer.write(sb.toString().trim());
        writer.close();
    }
}

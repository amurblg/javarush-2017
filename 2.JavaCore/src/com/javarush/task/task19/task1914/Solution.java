package com.javarush.task.task19.task1914;

/* 
Решаем пример
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream originalStream = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream decoyStream = new PrintStream(baos);
        System.setOut(decoyStream);

        testString.printSomething();
        String equaition = baos.toString();
        String[] operands = equaition.split("[+\\-*=]");
        int result = 0;
        if (equaition.indexOf("+") != -1)
            result = Integer.parseInt(operands[0].trim()) + Integer.parseInt(operands[1].trim());
        else if (equaition.indexOf("-") != -1)
            result = Integer.parseInt(operands[0].trim()) - Integer.parseInt(operands[1].trim());
        else if (equaition.indexOf("*") != -1)
            result = Integer.parseInt(operands[0].trim()) * Integer.parseInt(operands[1].trim());

        System.setOut(originalStream);
        System.out.println(equaition.trim() + " " + result);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}


package com.javarush.task.task19.task1909;

/* 
Замена знаков
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        reader.close();

        List<String> list = new ArrayList<>();
        BufferedReader reader2 = new BufferedReader(new FileReader(fileName1));
        while (reader2.ready()) list.add(reader2.readLine().replaceAll("\\.", "\\!"));
        reader2.close();

        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName2));
        for (int i = 0; i < list.size(); i++) {
            writer.write(list.get(i));
        }
        writer.close();
    }
}

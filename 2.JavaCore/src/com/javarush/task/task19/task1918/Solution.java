package com.javarush.task.task19.task1918;

/* 
Знакомство с тегами
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();
//        String fileName = "k:/tagstest.txt";

        String targetTag = args[0];
//        String targetTag = "span";

        BufferedReader reader2 = new BufferedReader(new FileReader(fileName));
        StringBuilder sb = new StringBuilder();
        while (reader2.ready()) sb.append(reader2.readLine());
        reader2.close();

        String content = sb.toString();
        if (content.indexOf("<" + targetTag) == -1) return;

        content = content.substring(content.indexOf("<" + targetTag), content.lastIndexOf("</" + targetTag + ">") + 3 + targetTag.length());
        Map<Integer, Boolean> tagsIndexes = new TreeMap<>();//Карта, в которой key - индекс в строке, с которого начинается тег, а value - true, если это открывающий тег, и false, если закрывающий
        int delta = 0;
        while (content.indexOf("<" + targetTag, delta) != -1) {
            tagsIndexes.put(content.indexOf("<" + targetTag, delta), true);
            delta += 2 + targetTag.length();
        }
        delta = 0;
        while (content.indexOf("</" + targetTag + ">", delta) != -1) {
            tagsIndexes.put(content.indexOf("</" + targetTag + ">", delta), false);
            delta += 3 + targetTag.length();
        }

        Map<Integer, String> tags = new TreeMap<>();//Карта, в которой key - индекс начала тега в строке, а value - собственно тело тега
        while (tagsIndexes.size() > 0) {
            int nextStart = -1;
            for (Map.Entry<Integer, Boolean> entry : tagsIndexes.entrySet()) {
                if (entry.getValue()) {
                    nextStart = entry.getKey();
                    continue;
                } else if (nextStart != -1 && entry.getValue() == false) {
                    tags.put(nextStart, content.substring(nextStart, entry.getKey() + 3 + targetTag.length()));
                    tagsIndexes.remove(entry.getKey());
                    tagsIndexes.remove(nextStart);
                    break;
                }
            }
        }
        tags.forEach((k, v) -> System.out.println(v));
    }
}

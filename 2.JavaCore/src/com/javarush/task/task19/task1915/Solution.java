package com.javarush.task.task19.task1915;

/* 
Дублируем текст
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        PrintStream originalStream = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream decoyStream = new PrintStream(baos);
        System.setOut(decoyStream);

        testString.printSomething();
        String intercept = baos.toString();
        FileOutputStream fos = new FileOutputStream(fileName);
        fos.write(intercept.getBytes());
        fos.close();

        System.setOut(originalStream);
        System.out.println(intercept);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
        }
    }
}


package com.javarush.task.task19.task1910;

/* 
Пунктуация
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        reader.close();

        List<String> list = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(fileName1));
        while (br.ready()) list.add(br.readLine().replaceAll("\\p{Punct}", ""));
        br.close();

        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName2));
        for (int i = 0; i < list.size(); i++) {
            bw.write(list.get(i));
        }
        bw.close();
    }
}

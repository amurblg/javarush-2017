package com.javarush.task.task19.task1919;

/* 
Считаем зарплаты
*/

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        Map<String, Double> salaries = new TreeMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        while (reader.ready()) {
            String[] line = reader.readLine().trim().split(" +");
            if (salaries.containsKey(line[0]))
                salaries.replace(line[0], salaries.get(line[0]) + Double.parseDouble(line[1]));
            else salaries.put(line[0], Double.parseDouble(line[1]));
        }
        reader.close();
        salaries.forEach((k, v) -> System.out.println(k + " " + v));
    }
}

package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException, ParseException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        //BufferedReader reader = new BufferedReader(new FileReader("k:/people.txt"));
        while (reader.ready()) {
            String[] line = reader.readLine().trim().split(" +");
            String name = "";
            for (int i = 0; i < line.length - 3; i++) {
                name += line[i] + " ";
            }
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            Date date = sdf.parse(String.join("", line[line.length - 3].length() < 2 ? "0" + line[line.length - 3] : line[line.length - 3], line[line.length - 2].length() < 2 ? "0" + line[line.length - 2] : line[line.length - 2], line[line.length - 1]));
            PEOPLE.add(new Person(name.trim(), date));
        }
        reader.close();
    }
}

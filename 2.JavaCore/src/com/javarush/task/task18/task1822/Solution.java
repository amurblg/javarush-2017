package com.javarush.task.task18.task1822;

/* 
Поиск данных внутри файла
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
        while (reader.ready()) {
            String nextLine = reader.readLine();
            String[] tmp = nextLine.split(" ");
            if (tmp.length >= 4 && tmp[0].equals(args[0])) {
                System.out.println(nextLine);
                reader.close();
                return;
            }
        }
        reader.close();
    }
}

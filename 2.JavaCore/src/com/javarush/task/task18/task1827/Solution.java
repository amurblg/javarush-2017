package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws Exception {
/*
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
        ItemsList items = new ItemsList();
        while (fileReader.ready()) {
            items.parseAndAdd(fileReader.readLine());
        }
        fileReader.close();
        if ("-c".equals(args[0])) {
            String productName = args[1].trim();
            double price = Double.parseDouble(args[2].trim().replace(",", "."));
            int quantity = Integer.parseInt(args[3].trim());
            items.add(new Item(items.getMaxUsedIndex() + 1, productName, price, quantity));

            items.saveToFile(fileName);
        }
*/
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(System.in));
        String fileOut = bufRead.readLine();
        bufRead.close();
        List<String> list = new ArrayList<>();
        BufferedReader readIn = new BufferedReader(new FileReader(fileOut));
        String str;
        final Pattern pattern = Pattern.compile("^\\d+");
        Matcher matcher;
        long maxNum = 0;
        while (readIn.ready()) {
            str = readIn.readLine();
            list.add(str);
            matcher = pattern.matcher(str);
            long num = 0;
            while (matcher.find()) {
                num = Long.parseLong(matcher.group(0));
            }
            if (num > maxNum) {
                maxNum = num;
            }
        }
        readIn.close();
        if (args[0].equals("-c")) {
            list.add(String.format("%-8d%-30s%-8s%-4s", (++maxNum), args[1], args[2], args[3]));
        }
        BufferedWriter bufWrite = new BufferedWriter(new FileWriter(fileOut));
        for (String s : list) {
            bufWrite.write(s + "\n");
        }
        bufWrite.close();
    }
}

/*class Item {
    private int id;
    private String title;
    private double price;
    private int quantity;

    public Item(int id, String title, double price, int quantity) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return String.format("%-8s%-30s%-8.2f%-4s",
                Integer.toString(id).substring(0, Integer.toString(id).length() > 8 ? 8 : Integer.toString(id).length()),
                title.substring(0, title.length() > 30 ? 30 : title.length()),
                price,
                Integer.toString(quantity).substring(0, Integer.toString(quantity).length() > 4 ? 4 : Integer.toString(quantity).length())
        );
    }

    public int getId() {
        return id;
    }
}*/

/*
class ItemsList extends ArrayList<Item> {
    public int getMaxUsedIndex() {
        int max = 0;
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i).getId() > max) max = this.get(i).getId();
        }
        return max;
    }

    public void parseAndAdd(String unparsedString) {
        if (unparsedString == null || unparsedString.trim().length() == 0) return;
        int id = Integer.parseInt(unparsedString.substring(0, 8).trim());
        String productName = unparsedString.substring(8, 38).trim();
        double price = Double.parseDouble(unparsedString.substring(38, 46).trim().replace(",", "."));
        int quantity = Integer.parseInt(unparsedString.substring(46).trim());
        this.add(new Item(id, productName, price, quantity));
    }

    public void saveToFile(String fileName) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName)));
        int size = this.size();
        for (int i = 0; i < size; i++) {
            writer.write(this.get(i).toString());
            if (i < size - 1) writer.newLine();
        }
        writer.close();
    }
}*/

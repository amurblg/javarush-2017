package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader(args[0]);
        char[] buffer = new char[(int) new File(args[0]).length()];
        fileReader.read(buffer);
        fileReader.close();

        Map<Character, Integer> freqs = new HashMap<>();
        for (char c : buffer) {
            if (!freqs.containsKey(c)) freqs.put(c, 1);
            else freqs.put(c, freqs.get(c) + 1);
        }

        List<Character> sortedKeys = new ArrayList<>(freqs.keySet());
        Collections.sort(sortedKeys);

        for (int i = 0; i < sortedKeys.size(); i++) {
            System.out.println(sortedKeys.get(i) + " " + freqs.get(sortedKeys.get(i)));
        }
    }
}

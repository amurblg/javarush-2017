package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine();
        String f2 = reader.readLine();
        reader.close();

        FileInputStream fis = new FileInputStream(f1);
        byte[] b = new byte[(int) new File(f1).length()];
        fis.read(b);
        fis.close();
        FileOutputStream fos = new FileOutputStream(f2);
        for (int i = b.length - 1; i >= 0; i--) {
            fos.write(b[i]);
        }
        fos.close();
    }
}

package com.javarush.task.task18.task1803;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream(new Scanner(System.in).nextLine());
        Map<Integer, Long> freq = new HashMap<>();
        while (fis.available() > 0) {
            int i = fis.read();
            if (!freq.containsKey(i)) freq.put(i, 1L);
            else freq.put(i, freq.get(i) + 1);
        }
        fis.close();

        long maxFreq = 0;
        for (Integer integer : freq.keySet()) {
            if (freq.get(integer) > maxFreq) maxFreq = freq.get(integer);
        }
        for (Integer integer : freq.keySet()) {
            if (freq.get(integer) == maxFreq) System.out.print(integer + " ");
        }
    }
}

package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])));
        Pattern pattern = Pattern.compile("[a-zA-Z]");
        int counter = 0;
        while (reader.ready()) {
            String nextLine = reader.readLine();
            Matcher matcher = pattern.matcher(nextLine);
            while (matcher.find()) counter++;
        }
        reader.close();
        System.out.println(counter);
    }
}

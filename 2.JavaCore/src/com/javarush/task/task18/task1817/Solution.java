package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])));
        char[] buffer = new char[(int) new File(args[0]).length()];
        reader.read(buffer);
        reader.close();

        int counter = 0;
        for (char c : buffer) if (c == ' ') counter++;
        System.out.printf("%.2f", 100 * (counter / (1.0 * buffer.length)));
    }
}

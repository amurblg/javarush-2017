package com.javarush.task.task18.task1828;

/* 
Прайсы 2
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(System.in));
        String fileOut = bufRead.readLine();
        bufRead.close();
        List<String> list = new ArrayList<>();
        BufferedReader readIn = new BufferedReader(new FileReader(fileOut));
        String str;
        final Pattern pattern = Pattern.compile("^\\d+");
        Matcher matcher;
        long maxNum = 0;
        while (readIn.ready()) {
            str = readIn.readLine();
            list.add(str);
            matcher = pattern.matcher(str);
            long num = 0;
            while (matcher.find()) {
                num = Long.parseLong(matcher.group(0));
            }
            if (num > maxNum) {
                maxNum = num;
            }
        }
        readIn.close();
        if ("-u".equals(args[0])) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).length() >= 8 && Integer.parseInt(list.get(i).substring(0, 8).trim()) == Integer.parseInt(args[1])) {
                    list.set(i, String.format("%-8s%-30s%-8s%-4s", args[1], args[2], args[3], args[4]));
                    break;
                }
            }
        } else if ("-d".equals(args[0])) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).length() >= 8 && Integer.parseInt(list.get(i).substring(0, 8).trim()) == Integer.parseInt(args[1])) {
                    list.remove(i);
                    break;
                }
            }

        }
        BufferedWriter bufWrite = new BufferedWriter(new FileWriter(fileOut));
        for (String s : list) {
            bufWrite.write(s + "\n");
        }
        bufWrite.close();
    }
}
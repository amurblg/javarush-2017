package com.javarush.task.task18.task1804;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream(new Scanner(System.in).nextLine());
        Map<Integer, Long> freq = new HashMap<>();
        while (fis.available() > 0) {
            int i = fis.read();
            if (!freq.containsKey(i)) freq.put(i, 1L);
            else freq.put(i, freq.get(i) + 1);
        }
        fis.close();

        long minFreq = Long.MAX_VALUE;
        for (Integer integer : freq.keySet()) {
            if (freq.get(integer) < minFreq) minFreq = freq.get(integer);
        }
        for (Integer integer : freq.keySet()) {
            if (freq.get(integer) == minFreq) System.out.print(integer + " ");
        }
    }
}

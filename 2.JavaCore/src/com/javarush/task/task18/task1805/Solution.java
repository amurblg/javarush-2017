package com.javarush.task.task18.task1805;

import java.io.FileInputStream;
import java.util.*;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream(new Scanner(System.in).nextLine());
        Set<Integer> bytes = new HashSet<>();
        while (fis.available() > 0) bytes.add(fis.read());
        fis.close();

        List<Integer> sorted = new ArrayList<>(bytes);
        Collections.sort(sorted);
        sorted.forEach(e -> System.out.print(e + " "));
    }
}

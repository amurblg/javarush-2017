package com.javarush.task.task18.task1810;

/* 
DownloadException
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws DownloadException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            String name = reader.readLine();
            FileInputStream fis = new FileInputStream(name);
            if (fis.available()<1000){
                fis.close();
                reader.close();
                throw new DownloadException();
            }
            fis.close();
        }
    }

    public static class DownloadException extends Exception {

    }
}

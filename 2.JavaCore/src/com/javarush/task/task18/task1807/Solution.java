package com.javarush.task.task18.task1807;

/* 
Подсчет запятых
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream(new Scanner(System.in).nextLine());
        char comma = ',';
        int counter = 0;
        while (fis.available() > 0) {
            int i = fis.read();
            if (i == comma) counter++;
        }
        fis.close();
        System.out.println(counter);
    }
}

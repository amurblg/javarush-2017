package com.javarush.task.task18.task1802;

import java.io.FileInputStream;
import java.util.Scanner;

/* 
Минимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream(new Scanner(System.in).nextLine());
        int min = 256;
        while (fis.available() > 0) {
            int i = fis.read();
            if (i < min) min = i;
        }

        fis.close();
        System.out.println(min);
    }
}

package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String fileName1 = scanner.nextLine();
        String fileName2 = scanner.nextLine();
        String fileName3 = scanner.nextLine();

        FileWriter fileWriter = new FileWriter(fileName1);
        FileReader fileReader1 = new FileReader(fileName2);
        FileReader fileReader2 = new FileReader(fileName3);
        char[] buffer1 = new char[(int) new File(fileName2).length()];
        char[] buffer2 = new char[(int) new File(fileName3).length()];
        fileReader1.read(buffer1);
        fileReader1.close();
        fileReader2.read(buffer2);
        fileReader2.close();

        fileWriter.write(buffer1);
        fileWriter.write(buffer2);
        fileWriter.close();
    }
}

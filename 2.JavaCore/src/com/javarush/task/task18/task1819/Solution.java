package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String fileName1 = scanner.nextLine();
        String fileName2 = scanner.nextLine();

        char[] buffer1 = new char[(int)new File(fileName1).length()];
        FileReader fileReader1 = new FileReader(fileName1);
        fileReader1.read(buffer1);
        fileReader1.close();

        char[] buffer2 = new char[(int) new File(fileName2).length()];
        FileReader fileReader2 = new FileReader(fileName2);
        fileReader2.read(buffer2);
        fileReader2.close();

        FileWriter fileWriter = new FileWriter(fileName1);
        fileWriter.write(buffer2);
        fileWriter.write(buffer1);
        fileWriter.close();
    }
}

package com.javarush.task.task18.task1823;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String nextLine;
        while (!(nextLine = scanner.nextLine()).equals("exit")) {
            ReadThread thread = new ReadThread(nextLine);
            thread.start();
        }
    }

    public static class ReadThread extends Thread {
        private String fileName;

        public ReadThread(String fileName) {
            //implement constructor body
            this.fileName = fileName;
        }

        @Override
        public void run() {
            byte[] buffer = null;
            try {
                FileInputStream inputStream = new FileInputStream(fileName);
                buffer = new byte[(int) new File(fileName).length()];
                inputStream.read(buffer);
                inputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Map<Byte, Integer> freqs = new HashMap<>();
            for (int i = 0; i < buffer.length; i++) {
                if (!freqs.containsKey(buffer[i])) freqs.put(buffer[i], 1);
                else freqs.put(buffer[i], freqs.get(buffer[i]) + 1);
            }
            int max = 0;
            int maxByte = 0;
            for (Byte aByte : freqs.keySet()) {
                if (freqs.get(aByte) > max) {
                    max = freqs.get(aByte);
                    maxByte = aByte;
                }
            }

            resultMap.put(fileName, maxByte);
        }
    }
}

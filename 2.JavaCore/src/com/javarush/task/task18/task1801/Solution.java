package com.javarush.task.task18.task1801;

import java.io.FileInputStream;
import java.util.Scanner;

/* 
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        FileInputStream fis = new FileInputStream(new Scanner(System.in).nextLine());
        int max = 0;
        while (fis.available() > 0) {
            int i = fis.read();
            if (i > max) max = i;
        }
        fis.close();
        System.out.println(max);
    }
}

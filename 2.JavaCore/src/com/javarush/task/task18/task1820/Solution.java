package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String fileName1 = scanner.nextLine();
        String fileName2 = scanner.nextLine();

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName1)));
        String[] fNumbers = reader.readLine().split(" +");
        reader.close();

        FileWriter fileWriter = new FileWriter(fileName2);
        for (int i = 0; i < fNumbers.length; i++) {
            fileWriter.write(String.valueOf(Math.round(Double.parseDouble(fNumbers[i]))));
            fileWriter.write(" ");
        }
        fileWriter.close();
    }
}

package com.javarush.task.task18.task1824;

/* 
Файлы и исключения
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String nextFileName = scanner.nextLine();

            try {
                FileInputStream fileInputStream = new FileInputStream(nextFileName);
                fileInputStream.close();
            } catch (FileNotFoundException e) {
                System.out.println(nextFileName);
                break;
            }
        }
    }
}

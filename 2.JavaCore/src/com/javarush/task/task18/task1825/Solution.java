package com.javarush.task.task18.task1825;

/* 
Собираем файл
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
//        Scanner scanner = new Scanner(new String("/home/john/King.Lion.avi.part1002\n" +
//                "/home/john/King.Lion.avi.part783\n" +
//                "/home/john/King.Lion.avi.part2\n" +
//                "/home/john/King.Lion.avi.part9\n" +
//                "end"));
        String nextFileName;
        String validFileName = "";
        Map<Integer, String> fileNames = new TreeMap<>();
        while (!(nextFileName = scanner.nextLine()).equals("end")) {
            validFileName = nextFileName;
            Integer index = Integer.valueOf(nextFileName.substring(nextFileName.lastIndexOf(".part") + 5));
            fileNames.put(index, nextFileName);
        }

//        FileWriter fileWriter = new FileWriter(fileNames.get(1).substring(0, fileNames.get(1).lastIndexOf(".part")));
        FileOutputStream outputStream = new FileOutputStream(validFileName.substring(0, validFileName.lastIndexOf(".part")));
//        System.out.println("<" + validFileName.substring(0, validFileName.lastIndexOf(".part")) + ">");//ensure that target filename is correct
        for (Integer i : fileNames.keySet()) {
            FileInputStream inputStream = new FileInputStream(fileNames.get(i));
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            outputStream.write(buffer);
//            FileReader fileReader = new FileReader(fileNames.get(i));
//            char[] buffer = new char[(int) new File(fileNames.get(i)).length()];
//            fileReader.read(buffer);
//            fileWriter.write(buffer);
//            fileReader.close();
        }

//        fileWriter.close();
        outputStream.close();
    }
}

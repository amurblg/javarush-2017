package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        FileInputStream fi;
        FileOutputStream fo;

        try {

            switch (args[0]) {
                case "-e":
                    fi = new FileInputStream(args[1]);
                    fo = new FileOutputStream(args[2]);
                    byte[] buf = new byte[fi.available()];
                    fi.read(buf);
                    for (int i = 0; i < buf.length; i++) {
                        fo.write(buf[i] + 1);
                    }
                    fi.close();
                    fo.close();
                    break;

                case "-d":
                    fi = new FileInputStream(args[1]);
                    fo = new FileOutputStream(args[2]);
                    byte[] fub = new byte[fi.available()];
                    fi.read(fub);
                    for (int i = 0; i < fub.length; i++) {
                        fo.write(fub[i] - 1);
                    }
                    fi.close();
                    fo.close();
            }
        } catch (Exception e) {

        }
    }
}

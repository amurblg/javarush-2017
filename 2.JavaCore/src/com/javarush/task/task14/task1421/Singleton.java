package com.javarush.task.task14.task1421;

/**
 * Created by Registered on 10.07.2017.
 */
public final class Singleton {
    private static Singleton instance = null;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instance == null) instance = new Singleton();
        return instance;
    }
}

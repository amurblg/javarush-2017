package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.util.InputMismatchException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        if (a <= 0 || b <= 0) throw new InputMismatchException();
        System.out.println(a > b ? recursive(a, b) : recursive(b, a));
    }

    public static int recursive(int max, int min) {
        if (min == 1) return 1;
        if (max % min == 0) return min;
        else return recursive(min, max % min);
    }
}

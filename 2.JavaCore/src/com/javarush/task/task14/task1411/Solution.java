package com.javarush.task.task14.task1411;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/* 
User, Looser, Coder and Proger
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Person person = null;
        String key = null;

        //тут цикл по чтению ключей, пункт 1
        List<String> possibleKeys = Arrays.asList("user", "loser", "coder", "proger");
        while (possibleKeys.contains(key = reader.readLine())) {
            //создаем объект, пункт 2
            if ("user".equals(key)) {
                person = new Person.User();
            } else if ("loser".equals(key)) {
                person = new Person.Loser();
            } else if ("coder".equals(key)) {
                person = new Person.Coder();
            } else if ("proger".equals(key)) {
                person = new Person.Proger();
            }
            doWork(person); //вызываем doWork

        }
    }

    public static void doWork(Person person) {
        // пункт 3
        if (person instanceof Person.User) ((Person.User) person).live();
        else if (person instanceof Person.Loser) ((Person.Loser) person).doNothing();
        else if (person instanceof Person.Coder) ((Person.Coder) person).coding();
        else if (person instanceof Person.Proger) ((Person.Proger) person).enjoy();

    }
}

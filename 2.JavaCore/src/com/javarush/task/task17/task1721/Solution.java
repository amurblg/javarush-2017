package com.javarush.task.task17.task1721;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String fileName1 = reader.readLine();
            String fileName2 = reader.readLine();
            reader.close();

            BufferedReader reader1 = new BufferedReader(new InputStreamReader(new FileInputStream(fileName1)));
            while (reader1.ready()) allLines.add(reader1.readLine());
            reader1.close();
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(new FileInputStream(fileName2)));
            while (reader2.ready()) forRemoveLines.add(reader2.readLine());
            reader2.close();
        } catch (IOException e) {
        }

        Solution s = new Solution();
        try {
            s.joinData();
        } catch (CorruptedDataException e) {
        }
    }

    public void joinData() throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)) allLines.removeAll(forRemoveLines);
        else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}

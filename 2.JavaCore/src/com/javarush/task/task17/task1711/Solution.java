package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        //start here - начни тут
        switch (args[0]) {
            case "-c":
                synchronized (allPeople) {
                    for (int i = 0; i < args.length / 3; i++) {
                        if ("м".equals(args[i * 3 + 2]))
                            allPeople.add(Person.createMale(args[i * 3 + 1], new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(args[i * 3 + 3])));
                        else
                            allPeople.add(Person.createFemale(args[i * 3 + 1], new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(args[i * 3 + 3])));
                        System.out.println(allPeople.size() - 1);
                    }
                    break;
                }
            case "-u":
                synchronized (allPeople) {
                    for (int i = 0; i < args.length / 4; i++) {
                        Person p = allPeople.get(Integer.parseInt(args[i * 4 + 1]));
                        p.setName(args[i * 4 + 2]);
                        p.setSex("м".equals(args[i * 4 + 3]) ? Sex.MALE : Sex.FEMALE);
                        p.setBirthDay(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(args[i * 4 + 4]));
                        allPeople.set(Integer.parseInt(args[i * 4 + 1]), p);
                    }
                    break;
                }
            case "-d":
                synchronized (allPeople) {
                    for (int i = 0; i < args.length - 1; i++) {
                        allPeople.get(Integer.parseInt(args[i + 1])).setBirthDay(null);
                        allPeople.get(Integer.parseInt(args[i + 1])).setSex(null);
                        allPeople.get(Integer.parseInt(args[i + 1])).setName(null);
                    }
                    break;
                }
            case "-i":
                synchronized (allPeople) {
                    for (int i = 0; i < args.length - 1; i++) {
                        Person p = allPeople.get(Integer.parseInt(args[i + 1]));
                        System.out.println(p.getName() + " " + (p.getSex() == Sex.MALE ? "м" : "ж") + " " + new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).format(p.getBirthDay()));
                    }
                    break;
                }
        }

    }
}
package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        //start here - начни тут
        if ("-c".equals(args[0]) && args.length == 4) {
            if ("м".equals(args[2]))
                allPeople.add(Person.createMale(args[1], new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(args[3])));
            else
                allPeople.add(Person.createFemale(args[1], new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(args[3])));
            System.out.println(allPeople.size() - 1);
        } else if ("-u".equals(args[0]) && args.length == 5) {
            Person p = allPeople.get(Integer.parseInt(args[1]));
            p.setName(args[2]);
            p.setSex("м".equals(args[3]) ? Sex.MALE : Sex.FEMALE);
            p.setBirthDay(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(args[4]));
            allPeople.set(Integer.parseInt(args[1]), p);
        } else if ("-d".equals(args[0]) && args.length == 2) {
            allPeople.get(Integer.parseInt(args[1])).setBirthDay(null);
            allPeople.get(Integer.parseInt(args[1])).setSex(null);
            allPeople.get(Integer.parseInt(args[1])).setName(null);
        } else if ("-i".equals(args[0]) && args.length == 2) {
            Person p = allPeople.get(Integer.parseInt(args[1]));
            System.out.println(p.getName() + " " + (p.getSex() == Sex.MALE ? "м" : "ж") + " " + new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH).format(p.getBirthDay()));
        }
    }
}

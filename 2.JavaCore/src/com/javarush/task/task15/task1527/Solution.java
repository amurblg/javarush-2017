package com.javarush.task.task15.task1527;

import java.util.Scanner;

/* 
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) {
        //add your code here
        String line = new Scanner(System.in).nextLine();
        String[] params = line.substring(line.indexOf("?") + 1).split("&");
        String objValue = "";
        for (int i = 0; i < params.length; i++) {
            System.out.print(params[i].split("=")[0] + " ");
            if ("obj".equals(params[i].split("=")[0])) objValue = params[i].split("=")[1];
        }
        System.out.println();
        if (objValue.length() > 0) {
            if (isValidDouble(objValue)) alert(Double.parseDouble(objValue));
            else alert(objValue);
        }
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }

    public static boolean isValidDouble(String d) {
        try {
            Double.parseDouble(d);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

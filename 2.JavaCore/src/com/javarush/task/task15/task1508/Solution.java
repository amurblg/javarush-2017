package com.javarush.task.task15.task1508;

/* 
ООП - Перегрузка - убираем лишнее
*/

public class Solution {
    public static void main(String[] args) {
        print(1);//Будет вызван метод print(long l)!
    }

    public static void print(long l) {
        System.out.println("Я буду Java прогером!");
    }
}

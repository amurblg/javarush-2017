package com.javarush.task.task15.task1524;

/* 
Порядок загрузки переменных
*/

public class Solution {
    static {
        init();
    }

    static {
        System.out.println("Static block");//1
    }

    {
        System.out.println("non-static block");//4
        printAllFields(this);//5-->
    }

    public int i = 6;

    public String name = "First name";


    public Solution() {
        System.out.println("Solution constructor");//8
        printAllFields(this);//9-->
    }

    public static void init() {
        System.out.println("static void init()");//2
    }

    public static void main(String[] args) {
        System.out.println("public static void main");//3
        Solution s = new Solution();
    }

    public static void printAllFields(Solution obj) {
        System.out.println("static void printAllFields");//-->5  //-->9
        System.out.println(obj.i);//7 (i==0)  //11 (i==6)
        System.out.println(obj.name);//6 (name==null)  //10 (name=="first name")
    }
}

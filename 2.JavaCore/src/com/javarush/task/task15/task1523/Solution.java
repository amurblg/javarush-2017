package com.javarush.task.task15.task1523;

/* 
Перегрузка конструкторов
*/

public class Solution {
    public Solution() {

    }

    Solution(int a) {

    }

    protected Solution(long a) {

    }

    private Solution(double a) {

    }

    public static void main(String[] args) {

    }
}


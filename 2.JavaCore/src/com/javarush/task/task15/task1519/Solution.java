package com.javarush.task.task15.task1519;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напиште тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nextLine;
        while (!"exit".equals(nextLine = reader.readLine())) {
            if (nextLine.contains(".") && isValidDouble(nextLine)) print(Double.parseDouble(nextLine));
            else if (isValidShort(nextLine)) {
                short sh = Short.parseShort(nextLine);
                if (sh > 0 && sh < 128) print(sh);
                else if (isValidInt(nextLine)) {
                    Integer i = Integer.valueOf(nextLine);
                    if (i <= 0 || i >= 128) print(i);
                }
            } else if (isValidInt(nextLine)) {
                Integer i = Integer.valueOf(nextLine);
                if (i <= 0 || i >= 128) print(i);
            } else print(nextLine);
        }

        reader.close();
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }

    static boolean isValidDouble(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    static boolean isValidShort(String s) {
        try {
            Short.parseShort(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    static boolean isValidInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

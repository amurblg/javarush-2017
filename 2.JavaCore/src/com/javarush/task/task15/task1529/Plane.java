package com.javarush.task.task15.task1529;

/**
 * Created by Registered on 10.07.2017.
 */
public class Plane implements Flyable {
    private int capacity;

    public Plane(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public void fly() {

    }
}
